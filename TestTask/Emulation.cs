﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TestTask
{

	#region Class: Emulation

	/// <summary>
	/// Emulates the work.
	/// </summary>
	public static class Emulation
	{

		#region Method: Public

		/// <summary>
		/// Runs the emulation.	
		/// </summary>
		/// <param name="clientsCount">Count of clients.</param>
		/// <param name="tasksPerClient">Count of tasks per client.</param>
		public static void Run(int clientsCount, int tasksPerClient) {
			for (int clientNo = 0; clientNo < clientsCount; clientNo++) {
				var n = clientNo;//disclosures a value.
				ThreadPool.QueueUserWorkItem(state => {
					var executor = SequentialExecutor.GetInstance();
					for (int taskNo = 0; taskNo < tasksPerClient; taskNo++) {
						var task = new Task(Console.WriteLine, $"Client #{n}: task#{taskNo}");
						executor.AddTask(task);
					}
				}, null);
			}
		}

		#endregion

	}

	#endregion
}
