﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TestTask
{
	#region Class: Executor

	/// <summary>
	/// A thread-safe tasks executor. Processes tasks sequentially in order they were added.
	/// Supports dynamic tasks adding. The working unit initializes using double-checked locking pattern.
	/// </summary>
	public class SequentialExecutor
	{
		#region Fields: Private

		/// <summary>
		/// Instance creation lock.
		/// </summary>
		private static object _instanceCreationLock = new object();

		/// <summary>
		/// _isRunning property lock.
		/// </summary>
		private static object _isRunningLock = new object();

		/// <summary>
		/// An instance of a <see cref="SequentialExecutor"/>.
		/// </summary>
		private static SequentialExecutor _instance;

		/// <summary>
		/// An execution queue.
		/// </summary>
		private readonly ConcurrentQueue<Task> _queue;

		/// <summary>
		/// Indicates whether executor is running.
		/// </summary>
		private bool _isRunning;

		#endregion

		#region Constructors: Private

		/// <summary>
		/// Initializes a new instance of <see cref="SequentialExecutor"/>. 
		/// </summary>
		private SequentialExecutor() {
			_queue = new ConcurrentQueue<Task>();
		}

		#endregion

		#region Methods: Private

		/// <summary>
		/// Begins the task queue invokation in a new CLR thread.
		/// </summary>
		private void Run() {
			ThreadPool.QueueUserWorkItem(InvokeQueue);
		}

		/// <summary>
		/// Sequentially invokes each task of the queue (<see cref="_queue"/>). 
		/// </summary>
		/// <param name="state">State object.</param>
		private void InvokeQueue(object state) {
			_isRunning = true;
			while (_queue.Any()) {
				Task task;
				if (_queue.TryDequeue(out task)) {
					task.Start();
					task.Wait();
				}
			}
			_isRunning = false;
		}

		#endregion

		#region Methods: Public

		/// <summary>
		/// Gets the instance of a <see cref="SequentialExecutor"/>.
		/// </summary>
		public static SequentialExecutor GetInstance() {
			if (_instance == null) {
				lock (_instanceCreationLock) {
					if (_instance == null) {
						_instance = new SequentialExecutor();
					}
				}
			}
			return _instance;
		}

		/// <summary>
		/// Adds a task to execution queue.		
		/// </summary>
		/// <param name="Task">Task to be executed.</param>
		public void AddTask(Task task) {
			_queue.Enqueue(task);
			lock (_isRunningLock) {
				if (!_isRunning) {
					Run();
				}
			}
		}

		#endregion

	}

	#endregion

}